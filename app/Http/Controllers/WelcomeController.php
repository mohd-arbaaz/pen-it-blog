<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
        /*$search = \request('search');
        if($search) {
            $posts =  Post::where('title' , 'like' , "%$search%")->simplePaginate(2);
        }
        else{
            $posts = Post::simplePaginate(2);
        }
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => $posts
        ]);*/
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => Post::search()->published()->latest('published_at')->simplePaginate(2)
        ]);
    }

    public function category(Category $category) {
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => $category->posts()->search()->published()->latest('published_at')->simplePaginate(2)
        ]);
    }
    public function tag(Tag $tag) {
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => $tag->posts()->search()->published()->latest('published_at')->simplePaginate(2)
        ]);
    }
    public function show(Post $post){
        $categories = Category::all();
        $tags = Tag::all();
        return \view('blog.post', \compact([
            'categories',
            'tags',
            'post'
        ]));
    }
}
