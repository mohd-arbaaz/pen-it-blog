<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact([
            'categories'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        //1. Validate data - done

        // 2. Store the data in Database
        Category::create([
            'name' => $request->name
        ]);

        // 3. Session set something and then return a view
        session()->flash('success', 'Category Added Successfully!');
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }
    public function edit(Category $category)
    {
        return view('categories.edit', compact([
            'category'
        ]));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        $category->save();
        // $category->update(['name' => $request->name]);
        session()->flash('success', 'Category Updated Succesfully!');
        return redirect(route('categories.index'));
    }

    public function destroy(Category $category)
    {
        if($category->posts->count() > 0){
            session()->flash('error', 'Category cannot be deleted as it is associated with some posts');
            return \redirect()->back();
        }
        $category->delete();
        session()->flash('success', 'Category Deleted Successfully!');
        return redirect(route('categories.index'));
    }
}
