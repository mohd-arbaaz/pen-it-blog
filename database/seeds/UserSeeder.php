<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'm.arbaz0608@gmail.com')->get()->first();
        if(!$user){
            User::create([
                'name' => 'Mohd Arbaaz',
                'email' => 'm.arbaz0608@gmail.com',
                'password' => Hash::make('abcd1234'),
                'role' => 'admin'
            ]);
        }else{
            $user->update(['role' => 'admin']);
        }

        User::create([
            'name' => 'Prem Mirani',
            'email' => 'miraniprem@gmail.com',
            'password' => Hash::make('abcd1234'),
        ]);

        User::create([
            'name' => 'Yasir Khan',
            'email' => 'khanyasir@gmail.com',
            'password' => Hash::make('abcd1234'),
        ]);
    }
}
