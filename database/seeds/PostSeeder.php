<?php

use App\Category;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News']);
        $categoryDesign = Category::create(['name' => 'Design']);
        $categoryTechnology = Category::create(['name' => 'Technology']);
        $categoryEngineering = Category::create(['name' => 'Engineering']);

        $tagCustomers = Tag::create(['name'=> 'customers']);
        $tagDesign = Tag::create(['name'=> 'design']);
        $tagLaravel = Tag::create(['name'=> 'laravel']);
        $tagCoding = Tag::create(['name'=> 'coding']);

        $post1 = Post::create([
            'title' => 'We relocated our office to our HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/1.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d'),
        ]);
        $post2 = Post::create([
            'title' => 'Engineers in trouble after pandemic!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/2.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d'),
        ]);
        $post3 = Post::create([
            'title' => 'Covid-19 vaccine has been Invented',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/3.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
        ]);
        $post4 = Post::create([
            'title' => 'PS5 final look revealed by Sony',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/4.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d'),
        ]);
        $post5 = Post::create([
            'title' => 'How to become a successful Java Developer',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/5.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
        ]);

        $post1->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post2->tags()->attach([$tagCustomers->id, $tagDesign->id, $tagCoding->id]);
        $post3->tags()->attach([$tagDesign->id]);
        $post4->tags()->attach([$tagCustomers->id, $tagCoding->id]);
        $post5->tags()->attach([$tagCustomers->id, $tagDesign->id, $tagCoding->id, $tagLaravel->id]);
    }
}
