@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('posts.create') }}" class="btn btn-outline-primary">Add Post</a>
    </div>
    <div class="card">
        <div class="card-header">Posts</div>

        <div class="card-body">
            @if ($posts->count() > 0)
                <table class="table">
                    <thead class="thead-light">
                        <th>Image</th>
                        <th>Title</th>
                        <th>Excerpt</th>
                        <th>Category</th>
                        <th>Tags</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($posts as $post)
                            <tr>
                                <td><img src="{{ asset('storage/'.$post->image) }}" alt="Post Image" width="128"></td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->excerpt }}</td>
                                <td>{{$post->category->name }}</td>
                                <td>
                                    @foreach ($post->tags as $tag)
                                        <a href="">{{ $tag->name }}</a>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-outline-secondary">Edit</a>
                                    <a href="#" onclick="displayModalForm({{$post}})" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#deleteModal" >Trash</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h5>No Posts to Show</h5>
            @endif

        </div>
        <div class="card-footer">
            {{ $posts->links() }}
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE');
                    <div class="modal-body">
                        <p>Are you sure you want to delete the post?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delete Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($post){
            var url = '/trash/' + $post.id;
            $('#deleteForm').attr('action', url);
        }
    </script>
@endsection
